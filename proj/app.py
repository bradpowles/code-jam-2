import data_io
from flask import Flask, jsonify, render_template, request

app = Flask(__name__)

PATH = "./data/data.json"

data = data_io.Handler(PATH)


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not found'
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


@app.route('/api/<entity_type>', methods=['POST', 'GET'])
def get_entities(entity_type):
    req = request.json or {"name": ""}
    entity_name = req.get('name')

    for entities in data.entities():

        if entities == entity_type:

            if entity_name == "":
                resp = jsonify(getattr(data, entities)())
                resp.status_code = 200

                return resp

            for entity in getattr(data, entities)():
                if entity['name'].upper() == entity_name.upper():
                    resp = jsonify(entity)
                    resp.status_code = 200

                    return resp

    return not_found()


@app.route('/api/<entity_type>/<name>', methods=['GET'])
def get_entity(entity_type, name):
    for entities in data.entities():

        if entities == entity_type:

            for entity in getattr(data, entities)():

                if entity['name'].upper() == name.upper() or str(entity['id']) == name:
                    resp = jsonify(entity)
                    resp.status_code = 200

                    return resp

    return not_found()


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run()
