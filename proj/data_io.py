import json


def data(path):
    with open(path) as f:
        return json.loads(f.read())


class Handler:
    def __init__(self, path):
        self.path = path

    def entities(self):
        response = []
        for entity in data(self.path)["entities"]:
            response.append(entity)
        return response

    def gods(self):
        response = []
        for entity in data(self.path)["entities"]["gods"]:
            response.append(entity)
        return response

    def titans(self):
        response = []
        for entity in data(self.path)["entities"]["titans"]:
            response.append(entity)
        return response

    def creatures(self):
        response = []
        for entity in data(self.path)["entities"]["creatures"]:
            response.append(entity)
        return response

    def heroes(self):
        response = []
        for entity in data(self.path)["entities"]["heroes"]:
            response.append(entity)
        return response
